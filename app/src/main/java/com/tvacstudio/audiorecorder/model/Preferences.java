package com.tvacstudio.audiorecorder.model;

import android.app.Activity;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class Preferences {

    private Activity activity;
    SharedPreferences preferences;

    String HOST;
    String USER;
    String PASS;

    public Preferences(Activity activity) {
        this.activity          = activity;
        this.preferences       = activity.getSharedPreferences("DATA_FTP", MODE_PRIVATE);
        this.HOST              = preferences.getString("HOST", "");
        this.USER              = preferences.getString("USER", "");
        this.PASS              = preferences.getString("PASS", "");

    }

    public String getHOST() {
        return HOST;
    }

    public String getUSER() {
        return USER;
    }

    public String getPASS() {
        return PASS;
    }


    public void setElementPreference(String key, String value ) {

        SharedPreferences.Editor editor = this.preferences.edit();

        editor.putString ( key , value );
        editor.commit();

    }



}
