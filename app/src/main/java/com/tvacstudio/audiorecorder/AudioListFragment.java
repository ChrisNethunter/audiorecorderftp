package com.tvacstudio.audiorecorder;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tvacstudio.audiorecorder.model.Preferences;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.io.File;
import java.io.IOException;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class AudioListFragment extends Fragment implements AudioListAdapter.onItemListClick {

    private ConstraintLayout playerSheet;
    private BottomSheetBehavior bottomSheetBehavior;

    private RecyclerView audioList;
    private File[] allFiles;

    private AudioListAdapter audioListAdapter;

    private MediaPlayer mediaPlayer = null;
    private boolean isPlaying = false;

    private File fileToPlay = null;

    //UI Elements
    private ImageButton playBtn;
    private TextView playerHeader;
    private TextView playerFilename;
    private TextView uploadFilePlayer;

    private SeekBar playerSeekbar;
    private Handler seekbarHandler;
    private Runnable updateSeekbar;

    private MyFTPClientFunctions ftpclient = null;
    Preferences preferences;

    private static final String TAG = "RecordingActivity";
    private static final String TEMP_FILENAME = "TAGtest.txt";
    private Context cntx = null;
    private ProgressDialog pd;

    public AudioListFragment() {
        // Required empty public constructor
    }

    private Handler handler = new Handler() {

        public void handleMessage(android.os.Message msg) {


            if (pd != null && pd.isShowing()) {
                pd.dismiss();
            }

            if (msg.what == 2) {

                Toast.makeText( getContext() , "Audio subido correctamente!", Toast.LENGTH_LONG).show();

            }else {

                Toast.makeText(getContext() , "Ah ocurrido un error estableciendo la conexion!", Toast.LENGTH_LONG).show();

            }

        }

    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        preferences = new Preferences(getActivity());
        ftpclient = new MyFTPClientFunctions();
        cntx = getContext();

        connectToFTPAddress();

        return inflater.inflate(R.layout.fragment_audio_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        playerSheet = view.findViewById(R.id.player_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(playerSheet);
        audioList = view.findViewById(R.id.audio_list_view);

        playBtn = view.findViewById(R.id.player_play_btn);
        playerHeader = view.findViewById(R.id.player_header_title);
        playerFilename = view.findViewById(R.id.player_filename);
        uploadFilePlayer = view.findViewById(R.id.player_header_name);


        playerSeekbar = view.findViewById(R.id.player_seekbar);

        String path = getActivity().getExternalFilesDir("/").getAbsolutePath();
        File directory = new File(path);
        allFiles = directory.listFiles();

        audioListAdapter = new AudioListAdapter(allFiles, this);

        audioList.setHasFixedSize(true);
        audioList.setLayoutManager(new LinearLayoutManager(getContext()));
        audioList.setAdapter(audioListAdapter);

        bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if(newState == BottomSheetBehavior.STATE_HIDDEN){
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                //We cant do anything here for this app
            }
        });

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPlaying){
                    pauseAudio();
                } else {
                    if(fileToPlay != null){
                        resumeAudio();
                    }
                }
            }
        });

        uploadFilePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fileToPlay != null){

                    pd = ProgressDialog.show(getContext(), "", "Subiendo...",
                            true, false);

                    new Thread(new Runnable() {
                        public void run() {

                            boolean status = false;

                            status = ftpclient.ftpUpload(fileToPlay.getAbsolutePath(), fileToPlay.getName(), "/", getContext());

                            if (status == true) {
                                Log.d(TAG, "Upload success");
                                handler.sendEmptyMessage(2);
                            } else {
                                Log.d(TAG, "Upload failed");
                                handler.sendEmptyMessage(-1);
                            }

                        }
                    }).start();

                }else{
                    Toast.makeText(getContext(), "Selecciona un audio primero!",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        playerSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                pauseAudio();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                mediaPlayer.seekTo(progress);
                resumeAudio();
            }
        });

    }

    @Override
    public void onClickListener(File file, int position) {
        fileToPlay = file;

        /*pd = ProgressDialog.show(getContext(), "", "Uploading...",
                true, false);

        new Thread(new Runnable() {
            public void run() {

                boolean status = false;

                status = ftpclient.ftpUpload(fileToPlay.getAbsolutePath(), fileToPlay.getName(), "/", getContext());

                if (status == true) {
                    Log.d(TAG, "Upload success");
                    handler.sendEmptyMessage(2);
                } else {
                    Log.d(TAG, "Upload failed");
                    handler.sendEmptyMessage(-1);
                }

            }
        }).start();*/

        if(isPlaying){
            stopAudio();
            playAudio(fileToPlay);
        } else {
            playAudio(fileToPlay);
        }
    }

    private void pauseAudio() {

        mediaPlayer.pause();

        playBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.player_play_btn, null));

        isPlaying = false;
        seekbarHandler.removeCallbacks(updateSeekbar);
    }

    private void resumeAudio() {
        mediaPlayer.start();
        playBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.player_pause_btn, null));
        isPlaying = true;

        updateRunnable();
        seekbarHandler.postDelayed(updateSeekbar, 0);

    }

    private void stopAudio() {
        //Stop The Audio
        playBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.player_play_btn, null));
        playerHeader.setText("Stopped");
        isPlaying = false;
        mediaPlayer.stop();
        seekbarHandler.removeCallbacks(updateSeekbar);
    }

    private void playAudio(File fileToPlay) {

        mediaPlayer = new MediaPlayer();
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        try {
            mediaPlayer.setDataSource(fileToPlay.getAbsolutePath());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        playBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.player_pause_btn, null));
        playerFilename.setText(fileToPlay.getName());
        playerHeader.setText("Playing");
        //Play the audio
        isPlaying = true;
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stopAudio();
                playerHeader.setText("Finished");
            }
        });

        playerSeekbar.setMax(mediaPlayer.getDuration());

        seekbarHandler = new Handler();
        updateRunnable();
        seekbarHandler.postDelayed(updateSeekbar, 0);

    }

    private void updateRunnable() {
        updateSeekbar = new Runnable() {
            @Override
            public void run() {
                playerSeekbar.setProgress(mediaPlayer.getCurrentPosition());
                seekbarHandler.postDelayed(this, 500);
            }
        };
    }

    private void connectToFTPAddress() {


        final String host = preferences.getHOST();
        final String username = preferences.getUSER();
        final String password = preferences.getPASS() ;


        if (host.length() < 1) {

            Toast.makeText(cntx, "Please Enter Host Address!",Toast.LENGTH_LONG).show();

        } else if (username.length() < 1) {

            Toast.makeText(cntx, "Please Enter User Name!", Toast.LENGTH_LONG).show();

        } else if (password.length() < 1) {

            Toast.makeText(cntx, "Please Enter Password!", Toast.LENGTH_LONG).show();

        } else {



            new Thread(new Runnable() {
                public void run() {
                    boolean status = false;

                    status = ftpclient.ftpConnect(host, username, password, 21);


                    Log.d("STATUS", String.valueOf(status));

                    if (status == true) {

                        Log.d(TAG, "Connection Successxxxxx22222");



                    } else {

                        Log.d(TAG, "Connection Erro222222r");

                    }

                }
            }).start();

        }
    }
    @Override
    public void onStop() {
        super.onStop();
        if(isPlaying) {
            stopAudio();
        }
    }






}
